from django.db import models
from django.utils.translation import gettext_lazy as _


class UserType(models.IntegerChoices):
    EMPLOYEE = 0, _("employee")
    COMPANY = 1, _("company")


class Day(models.IntegerChoices):
    MONDAY = 1, _("Monday")
    TUESDAY = 2, _("Tuesday")
    WEDNESDAY = 3, _("Wednesday")
    THURSDAY = 4, _("Thursday")
    FRIDAY = 5, _("Friday")
    SATURDAY = 6, _("Saturday")
    SUNDAY = 7, _("Sunday")
