default_attrs = {'class': 'form-control form-group'}

dob_attrs = {
    'class': 'form-control form-group',
    'type': 'date',
    'max': '2003-12-31',
    'min': '1940-01-01',
    'required': True,
}

time_attrs = {
    'class': 'form-control form-group',
    'type': 'time',
    'step': '1800',
}

radioselect_attrs = {'class': 'without-bullet'}
