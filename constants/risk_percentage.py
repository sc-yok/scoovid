risk_percentage = {
    'cardiovascular': 10.5,
    'diabetes': 7.3,
    'chronic_respiratory': 6.3,
    'hypertension': 6.0,
    'cancer': 5.6,
}