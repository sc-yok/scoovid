from django.shortcuts import render

from company.models import Company


def landing_page(request):
    context = {'user': request.user} if request.user else {}
    return render(request, 'homepage.html', context)