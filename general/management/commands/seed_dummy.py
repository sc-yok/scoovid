from django.core.management.base import BaseCommand
from django_seed import Seed
from datetime import time, date, timedelta
import random

from company.models import Company, JobRequirement, Shift
from employee.models import Employee, MedicalHistory
from general.models import JobType, Industry
from user.models import User
from constants.risk_percentage import risk_percentage
from constants.model_choices import UserType
from utils.calculate_medical_risk import calculate_age_risk
from utils.company import generate_secret_code

class Command(BaseCommand):
    help = 'Seed dummy data to db'

    def handle(self, *args, **options):
        IT_INDUSTRY_ID = 3

        user = User.objects.create(
            first_name='PT. Scoovid Indonesia',
            is_staff=True,
            is_superuser=True,
            type=UserType.COMPANY,
            email='scoovid@gmail.com',
            password='tugaskelompoksc',
        )
        company = Company.objects.create(
            user=user,
            industry_id=IT_INDUSTRY_ID,
            employees_per_shift=20,
            employee_capacity=100,
            start_operation_day=1,
            end_operation_day=5,
        )
        company.secret_code = generate_secret_code(company.id)
        company.save()

        job_type_id_list = list(JobType.objects.filter(industry_id=IT_INDUSTRY_ID).values_list('id', flat=True))
        seeder = Seed.seeder()
        seeder.add_entity(User, 100, {
            'is_staff': False,
            'type': UserType.EMPLOYEE,
        })
        seeder.execute()

        all_job_types = list(JobType.objects.all())
        shift_employee_cnt = 0
        job_types = []
        while len(all_job_types) > 0:
            job_type = random.choice(all_job_types)
            num_of_employee = random.randint(1,10)
            if shift_employee_cnt + num_of_employee > 20:
                break
            shift_employee_cnt += num_of_employee
            job_types.append(JobRequirement(
                company=company,
                job_type=job_type,
                num_of_employee=num_of_employee
            ))
            all_job_types.remove(job_type)
        JobRequirement.objects.bulk_create(job_types)

        employees = []
        medical_histories = []
        company = Company.objects.last()
        cnt = 0
        for user in User.objects.all():
            if hasattr(user, 'employee') or hasattr(user, 'company'):
                continue
            cnt += 1
            employee = Employee(
                user=user,
                date_of_birth=self.generate_random_birth_date(),
                company=company,
                job_type_id=self.generate_random_job_type(job_type_id_list),
            )
            employees.append(employee)
            if (cnt >= 100):
                break
        Employee.objects.bulk_create(employees)

        cnt = 0
        for employee in Employee.objects.all():
            if hasattr(employee, 'medicalhistory'):
                continue
            cnt += 1
            medical_history = MedicalHistory(
                cardiovascular=random.choice([0.0, risk_percentage['cardiovascular']]),
                diabetes=random.choice([0.0, risk_percentage['diabetes']]),
                chronic_respiratory=random.choice([0.0, risk_percentage['chronic_respiratory']]),
                hypertension=random.choice([0.0, risk_percentage['hypertension']]),
                cancer=random.choice([0.0, risk_percentage['cancer']]),
                age_risk=calculate_age_risk(employee.age),
                employee=employee,
            )
            medical_histories.append(medical_history)
            if (cnt >= 100):
                break
        MedicalHistory.objects.bulk_create(medical_histories)

        start_times = [6,9,12]
        end_times =  [9,12,17]
        data = []
        for i in range(len(start_times)):
            for day in company.work_days:
                shift = Shift(
                    start_time=time(start_times[i]),
                    end_time=time(end_times[i]),
                    day=day,
                    company=company,
                )
                data.append(shift)
        Shift.objects.bulk_create(data)

    def generate_random_birth_date(self):
        start_date = date(1920, 1, 1)
        end_date = date(2020, 12, 4)

        time_between_dates = end_date - start_date
        days_between_dates = time_between_dates.days
        random_number_of_days = random.randrange(days_between_dates)
        random_date = start_date + timedelta(days=random_number_of_days)

        return random_date

    def generate_random_job_type(self, job_type_id_list):
        return random.choice(job_type_id_list)
