from django.contrib import admin
from .models import Industry, JobType


admin.site.register(Industry)
admin.site.register(JobType)
