from django.urls import path
from django.conf import settings
from .views import (
    landing_page,
)

app_name = 'home'

urlpatterns = [
    path('', landing_page, name='landing'),
]

if settings.DEBUG:
    urlpatterns += [
    ]
