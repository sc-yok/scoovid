import random
class Schedule:
    def __init__(self, num_of_intervals):
        self.num_of_intervals = num_of_intervals
        self.schedule = []
        for i in range(num_of_intervals):
            self.schedule.append(bool(random.getrandbits(1)))
        self.num_of_segments = 0

    def is_scheduled(self, interval):
        return self.schedule[interval]

    def set_schedule(self, interval, new_value):
        self.schedule[interval] = new_value
        left_val = self.schedule[interval-1] if interval > 0 else False
        right_val = self.schedule[interval+1] if interval+1 < self.num_of_intervals else False
        if left_val and right_val:
            self.num_of_segments -= 1
        elif not left_val and not right_val:
            self.num_of_segments += 1

    def __str__(self):
        res_str = ''
        for i in self.schedule:
            if i:
                res_str += '1'
            else:
                res_str += '0'
        return res_str

class Configuration:
    def __init__(self, num_of_employees = 0, num_of_intervals = 0):
        # Construct a new scheduling configuration
        self.schedules = []
        for i in range(num_of_employees):
            self.schedules.append(Schedule(num_of_intervals))

    def get_schedule(self, idx):
        return self.schedules[idx]

    def mutate(self, idx, new_schedule):
        self.schedules[idx] = new_schedule

    def __str__(self):
        res_str = ''
        for idx, i in zip(range(len(self.schedules)), self.schedules):
            res_str += 'Employee {}: {}\n'.format(idx, i)
        return res_str
