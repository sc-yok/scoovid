import random
from .base import Selector as BaseSelector

class TournamentSelector(BaseSelector):
    def select(self, target_number):
        winners = []
        number_of_employees = len(self.configs[0].get_configuration().schedules)
        for k in range(target_number):
            nominee_cnt = random.choice(range(2, min(number_of_employees, 10)))
            nominee = random.sample(self.configs, nominee_cnt)
            winner = max(nominee, key=lambda x: x.evaluate())
            winners.append(winner.get_configuration())
        return winners
