class Selector:
    def __init__(self, configs):
        #  configs should be a list of Evaluator object
        self.configs = configs

    def select(self, target_number):
        # Performs a selection process on all of the individuals
        # Should return an array of Configuration. The number of element in them should equal `target_number`
        return [ i.get_configuration() for i in self.configs[0:target_number] ]
