from .base import Evaluator as BaseEvaluator
from ai.data.configuration import Configuration

from utils.calculate_productivity import get_productivity
from utils.generate_transmission_table import get_timetable_risk

class ScoovidEvaluator(BaseEvaluator):

    def __init__(self, configuration, company, employees, shifts, num_of_shifts, job_requirements):
        employee_schedule = [ s.schedule for s in configuration.schedules ]
        medical_histories = [ e.medicalhistory for e in employees]
        self.productivity = get_productivity(
            employee_schedule, company, employees, num_of_shifts, job_requirements,
        )

        risk = get_timetable_risk(medical_histories, employee_schedule, shifts)
        self.risk_score = risk["risk_score"]
        self.individual_risks = risk["individual_risks"]

        self.num_of_shifts = num_of_shifts
        self.configuration = configuration
        self.value = self.get_fitness_function()

    def get_fitness_function(self):
        fitness_function = 0
        for i in range(self.num_of_shifts):
            productivity_shift = self.productivity[i]
            risk_score_shift = self.risk_score[i]
            try :
                heuristic_score = productivity_shift/risk_score_shift
            except(ZeroDivisionError) :
                return 0
            if risk_score_shift == 100 :
                return 0
            else :
                fitness_function += heuristic_score
        fitness_function = round(fitness_function/self.num_of_shifts, 2)
        return fitness_function

    def get_risk_score(self):
        return self.risk_score

    def get_individual_risks(self):
        return self.individual_risks

    def get_productivity(self):
        return self.productivity
