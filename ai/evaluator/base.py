class Evaluator:
    def __init__(self, configuration):
        self.configuration = configuration
        self.value = self.get_fitness_function()

    def get_fitness_function(self):
        # Override this function on subclasses
        return 0

    def evaluate(self):
        return self.value

    def get_configuration(self):
        return self.configuration
