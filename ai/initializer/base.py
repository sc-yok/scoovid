from ai.data.configuration import Configuration
import random

class Initializer:
    def __init__(self, num_of_employees, num_of_intervals, individual_cnt, employees_per_shift):
        self.num_of_employees = num_of_employees
        self.num_of_intervals = num_of_intervals
        self.number_of_individuals = individual_cnt
        self.employees_per_shift = employees_per_shift

    def initialize(self):
        generation = []
        for i in range(self.number_of_individuals):
            generation.append(self.create_random_configuration(self.employees_per_shift))
        return generation

    def create_random_configuration(self, employees_per_shift):
        conf = Configuration(self.num_of_employees, self.num_of_intervals)
        employees_active = range(employees_per_shift // 3, employees_per_shift+1)
        for i in range(self.num_of_intervals):
            num_active = random.choice(employees_active)
            frame = [True]*num_active + [False]*(self.num_of_employees-num_active)
            random.shuffle(frame)
            for idx, j in enumerate(frame):
                conf.get_schedule(idx).set_schedule(i, j)
        return conf
