import random
from ai.data.configuration import Configuration


class Reproductor:
    def __init__(self, parents, num_of_employees, num_of_intervals):
        # parents should be an array of configurations
        self.parents = parents
        self.number_of_employees = num_of_employees
        self.num_of_intervals = num_of_intervals

    def crossover(self):
        #  Performs a crossover between parents
        #  Returns one Configuration object
        child = Configuration(self.number_of_employees, self.num_of_intervals)

        for k in range(self.number_of_employees):
            configs = [ i.get_schedule(k) for i in self.parents ]
            child.mutate(k, random.choice(configs))
        return child

    def mutate(self, child):
        #  Mutates a Configuration object
        #  Returns a mutated object
        idx = random.choice(range(self.number_of_employees))
        #  Mutate one index only
        new_child = child
        new_schedule = new_child.schedules[idx]

        for i in range(self.num_of_intervals):
            new_schedule.set_schedule(i, random.choice([True, False]))

        new_child.mutate(idx, new_schedule)
        return new_child

    def reproduce(self):
        #  Performs reproductions process as a whole
        result = self.crossover()
        mutated_result = self.mutate(result)
        return mutated_result
