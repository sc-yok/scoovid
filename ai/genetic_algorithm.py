from .evaluator.fitness_function import ScoovidEvaluator
from .initializer import BaseInitializer
from .evaluator import BaseEvaluator
from .selector import TournamentSelector
from .reproductor import BaseReproductor
from employee.models import EmployeeShift
from company.models import Shift
import copy


class GeneticAlgorithm:
    def __init__(self, initializer, evaluator, selector, reproductor, individual_cnt, company):
        self.evaluator = evaluator
        self.selector = selector
        self.reproductor = reproductor
        self.evaluation_list = []

        #  Company Data
        self.company = company
        self.employees = company.employee_set.all()
        self.num_of_employees = self.employees.count()
        self.shifts = company.shift_set.all()
        self.num_of_shifts = self.shifts.count()
        self.job_requirements = company.jobrequirement_set.values_list(
            'job_type_id', 'num_of_employee',
        )

        #  Create the first generation
        self.individual_cnt = individual_cnt
        self.generation = initializer(
            self.num_of_employees, self.num_of_shifts, individual_cnt, self.company.employees_per_shift
        ).initialize()

    def iterate(self):
        #  Create the next generation based on the current generation
        #  You can do this several times
        prev_gen = copy.deepcopy(self.generation)
        next_gen = []

        #  Get the fitness function for each individuals
        self.evaluation_list = [
            self.evaluator(i, self.company, self.employees, self.shifts, self.num_of_shifts, self.job_requirements)
            for i in self.generation
        ]
        for i in range(self.individual_cnt):
            #  Select two fittest individuals
            parents = self.selector(self.evaluation_list).select(2)
            child = self.reproductor(parents, self.num_of_employees, self.num_of_shifts).reproduce()
            next_gen.append(child)

        inter_gen = prev_gen + next_gen
        inter_eval = [
            self.evaluator(i, self.company, self.employees, self.shifts, self.num_of_shifts, self.job_requirements)
            for i in inter_gen
        ]
        print("Previous + Next Generation: ")
        print([ i.evaluate() for i in inter_eval ])

        self.evaluation_list = sorted(inter_eval, key=lambda x: x.evaluate())[self.individual_cnt:]
        self.generation = [ i.get_configuration() for i in self.evaluation_list ]
        print("Best individuals: ", [ i.evaluate() for i in self.evaluation_list ])
        print()

    def get_latest_gen(self):
        return self.generation

    def get_fittest_conf(self):
        fittest_conf = max(self.evaluation_list, key=lambda x: x.evaluate())
        print("Risk Score: ", fittest_conf.get_risk_score())
        print("Productivity: ", fittest_conf.get_productivity())
        print()
        return {
            'config': fittest_conf.get_configuration(),
            'risk': fittest_conf.get_individual_risks(),
        }


def run_genetic_algorithm(company):
    ai = GeneticAlgorithm(BaseInitializer, ScoovidEvaluator, TournamentSelector, BaseReproductor, 10, company)
    for i in range(20):
        print("Iterating generation {}...".format(i))
        ai.iterate()
    print("Done!")

    employee_shifts = []
    fittest_conf = ai.get_fittest_conf()
    individual_picked = fittest_conf['config']
    risk_per_individual = fittest_conf['risk']
    for i in range(0, len(individual_picked.schedules)):
        schedule_obj = individual_picked.schedules[i]
        for j in range(0,len(schedule_obj.schedule)):
            if schedule_obj.schedule[j] == 1:
                employee_shift = EmployeeShift(
                    employee=ai.employees[i],
                    shift=ai.shifts[j],
                    risk_score=risk_per_individual[i][j],
                )
                employee_shifts.append(employee_shift)
    EmployeeShift.objects.bulk_create(employee_shifts)
