from django.urls import path
from .views import (
    employee_home,
    get_company_by_code,
    create_employee_profile,
    record_medical_history,
    view_company_schedule,
)

app_name = 'employee'

urlpatterns = [
    path('schedule/', employee_home, name='home'),
    path('company-schedule/', view_company_schedule, name='company_schedule'),
    path('profile/create/company/', get_company_by_code, name='get_company_by_code'),
    path('profile/create/', create_employee_profile, name='create_profile'),
    path('register/medical-history-data/', record_medical_history, name='medical_history_form'),
]
