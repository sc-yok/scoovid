from django.db import models
from datetime import date

class Employee(models.Model):
    user = models.OneToOneField('user.User', on_delete=models.CASCADE)
    date_of_birth = models.DateField(blank=True, null=True)
    company = models.ForeignKey('company.Company', on_delete=models.SET_NULL, null=True)
    job_type = models.ForeignKey('general.JobType', on_delete=models.SET_NULL, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @property
    def age(self):
        today_date = date.today()
        birth_date = self.date_of_birth
        age = today_date.year - birth_date.year - 1
        if ((today_date.month > birth_date.month) or ((today_date.month == birth_date.month) and (today_date.day >= birth_date.day))):
            age += 1

        return age

    @property
    def full_name(self):
        return self.user.get_full_name()

    @property
    def full_name_in_dash(self):
        return f'{"-".join(self.user.first_name.split())}-{"-".join(self.user.last_name.split())}'

    def __str__(self):
        return f'{self.company.name} - {self.user.get_full_name()}'


class MedicalHistory(models.Model):
    cardiovascular = models.FloatField(default=0.0)
    diabetes = models.FloatField(default=0.0)
    chronic_respiratory = models.FloatField(default=0.0)
    hypertension = models.FloatField(default=0.0)
    cancer = models.FloatField(default=0.0)
    age_risk = models.FloatField(default=0.0)

    employee = models.OneToOneField(Employee, on_delete=models.CASCADE, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.employee.user.get_full_name()}'


class EmployeeShift(models.Model):
    employee = models.ForeignKey('employee.Employee', on_delete=models.CASCADE)
    shift = models.ForeignKey('company.Shift', on_delete=models.CASCADE)
    risk_score = models.FloatField(default=-1.0)

    def __str__(self):
        return f'{self.shift}'
