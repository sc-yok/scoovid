from django.utils.translation import gettext_lazy as _
from django.forms import (
    ModelForm,
    DateInput,
    EmailInput,
    PasswordInput,
    TextInput,
    RadioSelect,
    Select,
    Form,
    ChoiceField,
    CharField,
)
from constants.form_attrs import (
    default_attrs,
    dob_attrs,
    radioselect_attrs,
)
from constants.form_choices import (
    yes_or_no_choices,
)
from .models import Employee


class EmployeeProfileForm(ModelForm):
    company_name = CharField(
        label=_("Company:"),
        widget=TextInput(attrs=default_attrs),
        disabled=True,
        required=False,
    )

    class Meta:
        model = Employee
        fields = [
            'company_name',
            'date_of_birth',
            'job_type',
        ]
        widgets = {
            'date_of_birth': DateInput(attrs=dob_attrs),
            'job_type': Select(attrs=default_attrs),
        }
        
    def __init__(self, *args, **kwargs):
        job_type_choices = []
        if 'job_type_choices' in kwargs:
            job_type_choices = kwargs.pop('job_type_choices')
        super(EmployeeProfileForm, self).__init__(*args, **kwargs)
        self.fields['job_type'].choices = job_type_choices


class MedicalHistoryForm(Form):
    cardiovascular = ChoiceField(label = 'Cardiovascular Disease?', widget = RadioSelect(attrs=radioselect_attrs), choices = yes_or_no_choices, initial=False)
    diabetes = ChoiceField(label = 'Diabetes?', widget = RadioSelect(attrs=radioselect_attrs), choices = yes_or_no_choices, initial=False)
    chronic_respiratory = ChoiceField(label = 'Chronic Respiratory?', widget = RadioSelect(attrs=radioselect_attrs), choices = yes_or_no_choices, initial=False)
    hypertension = ChoiceField(label = 'Hypertension?', widget = RadioSelect(attrs=radioselect_attrs), choices = yes_or_no_choices, initial=False)
    cancer = ChoiceField(label = 'Cancer?', widget = RadioSelect(attrs=radioselect_attrs), choices = yes_or_no_choices, initial=False)
