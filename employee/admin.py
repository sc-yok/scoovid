from django.contrib import admin
from .models import Employee, MedicalHistory, EmployeeShift


class MedicalHistoryInline(admin.TabularInline):
    model = MedicalHistory

class EmployeeShiftInline(admin.TabularInline):
    model = EmployeeShift

class EmployeeAdmin(admin.ModelAdmin):
    search_fields = [
        'user__email',
        'user__first_name',
    ]
    inlines = [
        MedicalHistoryInline,
        EmployeeShiftInline,
    ]


admin.site.register(Employee, EmployeeAdmin)
