from django.shortcuts import render, redirect, reverse
from django.http import JsonResponse
from .forms import EmployeeProfileForm, MedicalHistoryForm
from .models import MedicalHistory, Employee, EmployeeShift

from company.models import Company
from company.forms import CompanySecretCodeForm
from company.views import get_company_schedule
from general.models import JobType
from user.models import User
from user.permissions import employee_required
from utils.calculate_medical_risk import (
    calculate_age_risk,
    calculate_disease_risk,
)
from utils.clean_medical_history_input import (
    clean_medical_history_input,
)


@employee_required()
def view_company_schedule(request):
    company = request.user.employee.company
    return get_company_schedule(request, company)


@employee_required()
def employee_home(request):
    employee = request.user.employee
    employee_shift = EmployeeShift.objects.filter(employee=employee)
    converted_shifts = convert_shift_array(employee_shift)
    return render(request, 'dashboard/dashboard.html', {
        'shifts':converted_shifts, 
        'schedule': 'employee',
        'employee':employee
    })


def convert_shift_array(employee_shift):
    result = {}
    active_shift = set()
    for i in employee_shift:
        if (i.shift.day-1) not in active_shift :
            result[i.shift.day-1] = []
        result[i.shift.day-1].append(str(i.shift))
        active_shift.add(i.shift.day-1)
    return result


@employee_required()
def get_company_by_code(request):
    if hasattr(request.user, 'employee'):
        return redirect('employee:home')

    context = {
        'action': 'create',
        'url': reverse('employee:get_company_by_code'),
        'form': CompanySecretCodeForm,
    }

    if request.method == 'POST':
        try:
            company = Company.objects.get(secret_code=request.POST['secret_code'])
            employee = Employee.objects.create(
                company=company,
                user=request.user,
            )
            return redirect('employee:create_profile')
        except Company.DoesNotExist:
            context['error'] = 'Company does not exist. Please re-check the secret code with your company.'
    return render(request, 'form/profile.html', context)


@employee_required()
def create_employee_profile(request):
    employee = request.user.employee

    if request.method == 'POST':
        form = EmployeeProfileForm(request.POST)
        if form.is_valid():
            employee.date_of_birth = request.POST['date_of_birth']
            employee.job_type_id = request.POST['job_type']
            employee.save()
            return redirect('employee:medical_history_form')
    else:
        company = employee.company
        job_types = JobType.objects.filter(industry_id=company.industry_id)
        job_type_choices = [(j.id, j.name) for j in job_types]

        form = EmployeeProfileForm(
            initial={'company_name': company.name},
            job_type_choices=job_type_choices,
        )

    return render(request, 'form/profile.html', {
        'action': 'create',
        'url': reverse('employee:create_profile'),
        'form': form,
    })


@employee_required()
def record_medical_history(request):
    form = MedicalHistoryForm()
    employee = request.user.employee

    if request.method == 'POST':
        form = MedicalHistoryForm(request.POST)
        if form.is_valid():
            medical_history = MedicalHistory()
            cleaned_input = clean_medical_history_input(request.POST)
            medical_history.cardiovascular = calculate_disease_risk(cleaned_input, 'cardiovascular')
            medical_history.diabetes = calculate_disease_risk(cleaned_input, 'diabetes')
            medical_history.chronic_respiratory = calculate_disease_risk(cleaned_input, 'chronic_respiratory')
            medical_history.hypertension = calculate_disease_risk(cleaned_input, 'hypertension')
            medical_history.cancer = calculate_disease_risk(cleaned_input, 'cancer')
            medical_history.age_risk = calculate_age_risk(employee.age)
            medical_history.employee = employee
            medical_history.save()
            return redirect('employee:home')

    return render(request, 'form/medical_history.html', {
        'url': reverse('employee:medical_history_form'),
        'form': form,
    })
