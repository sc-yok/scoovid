const date = new Date();

const renderCalendar = () => {
  date.setDate(1);

  const monthDays = document.querySelector(".days");

  const lastDay = new Date(
    date.getFullYear(),
    date.getMonth() + 1,
    0
  ).getDate();

  const prevLastDay = new Date(
    date.getFullYear(),
    date.getMonth(),
    0
  ).getDate();

  const firstDayIndex = date.getDay();

  const lastDayIndex = new Date(
    date.getFullYear(),
    date.getMonth() + 1,
    0
  ).getDay();

  const nextDays = 7 - lastDayIndex - 1;

  const months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];

  document.querySelector(".date h1").innerHTML = months[date.getMonth()] + " " + date.getFullYear();

  document.querySelector(".date p").innerHTML = new Date().toDateString();
  let days = "";
  for (let x = firstDayIndex; x > 0; x--) {
    days += `<div class="prev-date">${prevLastDay - x + 1}</div>`;
  }
  for (let i = 1; i <= lastDay; i++) {
	day_of_week = new Date(date.getFullYear(), date.getMonth(), i).getDay();
	if (day_of_week === 0)day_of_week = 7; else day_of_week -= 1;
    if (
      i === new Date().getDate() &&
      date.getMonth() === new Date().getMonth() &&
	  date.getFullYear() === new Date().getFullYear()
    ) {
	  if (document.body.contains(document.getElementById(day_of_week))){
		days += `<div class="today" style=" background-color: #FF4133;"><span style="margin-left:20px"> ${i}</span> <div class="row" onclick="overlayon(${day_of_week})" style="margin-right:0px"><span class="dot"></span></div></div>`;
	  }
	  else
      days += `<div class="today">${i}</div>`;
    } else {
	  if (document.body.contains(document.getElementById(day_of_week))){
		days += `<div class="today" style=" background-color: #FF9B21;"><span style="margin-left:20px"> ${i}</span> <div class="row" onclick="overlayon('${day_of_week}')" style="margin-right:0px"><span class="dot"></span></div></div>`;
	  }
	  else
		days += `<div>${i}</div>`;
    }
  }

  for (let j = 1; j <= nextDays; j++) {
    days += `<div class="next-date">${j}</div>`;
    monthDays.innerHTML = days;
  }
};

document.querySelector(".prev").addEventListener("click", () => {
  date.setMonth(date.getMonth() - 1);
  renderCalendar();
});

document.querySelector(".next").addEventListener("click", () => {
  date.setMonth(date.getMonth() + 1);
  renderCalendar();
});

renderCalendar();