const company_shift_form = document.getElementById('company_shift_form');
const company_jobreq_form = document.getElementById('company_jobreq_form');

const job_types = company_jobreq_form.getAttribute('data-job-type-choices');
const job_types_json = parseJson(job_types);

$(document).ready(() => {
    const company_shifts = company_shift_form.getAttribute('data-shifts');
    const company_shifts_json = parseJson(company_shifts);
    if (company_shifts_json.length > 0) {
        company_shifts_json.map(({ start_time, end_time }, id) => {
            const param = id === 1 ? {
                start_time,
                end_time,
                required: 'required',
            } : {
                start_time,
                end_time,
            }
            addShiftRow(param);
        });
    } else {
        addShiftRow({required: 'required',});
    }

    const job_reqs = company_jobreq_form.getAttribute('data-job-reqs');
    const job_reqs_json = parseJson(job_reqs);
    job_reqs_json.map(({ job_type_id, num_of_employee }) => addJobReqRow({
        job_type_id,
        num_of_employee,
    }));
});

function mapJobReqOptions(job_type_id) {
    return job_types_json.map(({ id, name }) => `
        <option value="${id}" ${job_type_id === id && 'selected'}>${name}</option>
    `);
}

function parseJson(data) {
    return JSON.parse(data.replace(/'/g, '"'));
}

function addShiftRow({required = '', start_time = '', end_time = ''} = {}) {
    const shift_form_div = document.createElement('div');
    shift_form_div.className = 'form-row form-group';
    shift_form_div.style = 'align-items: center;'

    shift_form_div.innerHTML = `
    <div class="col">
        <div class="input-group">
            <input type="number" name="start_time" class="form-control"
                min="0" max="24" placeholder="08" ${required} value="${start_time}"/>
            <div class="input-group-append">
                <span class="input-group-text">:00</span>
            </div>
        </div>
    </div>
    --
    <div class="col">
        <div class="input-group">
            <input type="number" name="end_time" class="form-control"
                min="0" max="24" placeholder="17" ${required} value="${end_time}"/>
            <div class="input-group-append">
                <span class="input-group-text">:00</span>
            </div>
        </div>
    </div>
    <div class="col">
        <button type="button" class="btn btn-danger remove" onClick="removeShiftRow(this)">X</button>
    <div>
    `;

    company_shift_form.appendChild(shift_form_div);
}

function removeShiftRow(button) {
    company_shift_form.removeChild(button.parentNode.parentNode);
}

function addJobReqRow({job_type_id = '', num_of_employee = ''} = {}) {
    const jobreq_form_div = document.createElement('div');
    jobreq_form_div.className = 'form-row form-group';
    jobreq_form_div.style = 'align-items: center;'

    jobreq_form_div.innerHTML = `
    <div class="col-6">
        <select name="job_type" class="form-control">
            ${mapJobReqOptions(job_type_id)}
        </select>
    </div>
    :
    <div class="col">
        <input type="number" name="num_of_employee" class="form-control" min="0" 
            value="${num_of_employee}" />
    </div> employee(s)
    <div class="col">
        <button type="button" class="btn btn-danger remove" onClick="removeJobReqRow(this)">X</button>
    <div>
    `;

    company_jobreq_form.appendChild(jobreq_form_div);
}

function removeJobReqRow(button) {
    company_jobreq_form.removeChild(button.parentNode.parentNode);
}
