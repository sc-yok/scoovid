function overlayon(id) {
    overlay_back();
    overlay_fore(id);
}

function overlay_back() {
    var overlay = document.getElementById("overlay");
    overlay.style.visibility = "visible";
    overlay.style.opacity = "100";
}

// Activate overlay foreground
function overlay_fore(id) {
    var fore = document.getElementById(id);
    fore.style.visibility = "visible";
    fore.style.opacity = "100";
    fore.style.top = "50%";
}

function off() {
    var overlay = document.getElementById('overlay');
    overlay.style.visibility = "hidden";
    overlay.style.opacity = "0";
    var fore = document.getElementsByClassName("fore");
    for (var i = 0; i < fore.length; i++) {
        fore[i].style.visibility = "hidden"
        fore[i].style.opacity = "0"
        fore[i].style.top = "45%"
    }
}