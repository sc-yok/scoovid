from django.contrib.auth.forms import UserCreationForm
from django.utils.translation import gettext_lazy as _
from django.forms import (
    ModelForm,
    Form,
    CharField,
    EmailInput,
    PasswordInput,
    TextInput,
)
from constants.form_attrs import (
    default_attrs,
)
from .models import User


class SignupForm(UserCreationForm):
    password1 = CharField(
        label=_("Password"),
        strip=False,
        widget=PasswordInput(attrs=default_attrs),
    )
    password2 = CharField(
        label=_("Re-enter password"),
        strip=False,
        widget=PasswordInput(attrs=default_attrs),
    )

    class Meta:
        model = User
        fields = [
            'first_name',
            'last_name',
            'email',
            'password1',
            'password2',
        ]
        widgets = {
            'first_name': TextInput(attrs=default_attrs),
            'last_name': TextInput(attrs=default_attrs),
            'email': EmailInput(attrs=default_attrs),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['email'].widget.attrs.pop('autofocus', None)


class CompanySignupForm(SignupForm):
    class Meta:
        model = User
        fields = [
            'first_name',
            'email',
            'password1',
            'password2',
        ]
        widgets = {
            'first_name': TextInput(attrs=default_attrs),
            'email': EmailInput(attrs=default_attrs),
        }
        labels = {
            'first_name': _('Name'),
        }


class LoginForm(Form):
    email = CharField(label = 'Email', widget = EmailInput(attrs=default_attrs))
    password = CharField(label = 'Password', widget = PasswordInput(attrs=default_attrs))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['email'].widget.attrs.pop('autofocus', None)
