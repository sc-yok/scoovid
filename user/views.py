from django.shortcuts import render, redirect, reverse
from .forms import CompanySignupForm, SignupForm, LoginForm
from django.contrib.auth import login, logout, authenticate

from constants.model_choices import UserType


def signup_employee(request):
    form = SignupForm()
    if request.method == 'POST':
        form = SignupForm(request.POST)
        if form.is_valid():
            employee = form.save(commit=False)
            employee.save()
            login(request, employee)
            return redirect('employee:get_company_by_code')

    return render(request, 'form/signup.html', {
        'type': 'user',
        'url': reverse('user:signup_employee'),
		'form': form,
    })


def signup_company(request):
    form = CompanySignupForm()
    if request.method == 'POST':
        form = CompanySignupForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.type = UserType.COMPANY
            user.save()
            login(request, user)
            return redirect('company:create_profile')

    return render(request, 'form/signup.html', {
        'type': 'company',
        'url': reverse('user:signup_company'),
		'form': form,
    })


def login_view(request):
    form = LoginForm()
    error = ''
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            user = authenticate(email=request.POST['email'], password=request.POST['password'])
            if user != None:
                login(request, user)
                return redirect(f'{user.get_type_display()}:home')
            error = 'Login failed. Incorrect email or password.'
    
    return render(request, 'form/login.html', {
        'title': 'Login',
        'form': form,
        'url': reverse('user:login'),
        'error': error,
    })


def logout_view(request):
    if request.method == 'POST':
        logout(request)
        return redirect('home:landing')
