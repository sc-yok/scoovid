from django.contrib.auth.decorators import user_passes_test
from django.core.exceptions import PermissionDenied

from constants.model_choices import UserType


def company_required():
    def check_perms(user):
        if user.is_authenticated:
            if user.type == UserType.COMPANY:
                return True
            raise PermissionDenied
        return False
    return user_passes_test(check_perms)


def employee_required():
    def check_perms(user):
        if user.is_authenticated:
            if user.type == UserType.EMPLOYEE:
                return True
            raise PermissionDenied
        return False
    return user_passes_test(check_perms)
