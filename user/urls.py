from django.urls import path
from .views import (
    signup_employee,
    signup_company,
    login_view,
    logout_view,
)

app_name = 'user'

urlpatterns = [
    path('signup/', signup_employee, name='signup_employee'),
    path('signup/company/', signup_company, name='signup_company'),
    path('login/', login_view, name='login'),
    path('logout/', logout_view, name='logout'),
]
