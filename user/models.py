from django.db import models
from django.contrib.auth.models import AbstractUser, UserManager
from django.utils.translation import gettext_lazy as _
from constants.model_choices import UserType


def extract_username_from_email(email):
    return email.split('@')[0]


class CustomUserManager(UserManager):

    def create_user(self, username, email=None, password=None, **extra_fields):
        if not username:
            username = extract_username_from_email(email)
        return super().create_user(username, email, password, **extra_fields)

    def create_superuser(self, username=None, email=None, password=None, **extra_fields):
        if not username:
            username = extract_username_from_email(email)
        return super().create_superuser(username, email, password, **extra_fields)


class User(AbstractUser):
    username = models.CharField(
        _('username'),
        max_length=150,
        blank=True,
    )
    email = models.CharField(
        _('email'),
        max_length=255,
        unique=True,
        error_messages={
            'unique': _("A user with that email already exists."),
        },
    )
    type = models.IntegerField(choices=UserType.choices, default=UserType.EMPLOYEE)

    objects = CustomUserManager()
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
