# SCOOVID

[![pipeline status](https://gitlab.com/sc-yok/scoovid/badges/master/pipeline.svg)](https://gitlab.com/sc-yok/scoovid/-/commits/master)

## About Scoovid

Scoovid is an AI-powered, Django-based scheduling website for companies to find an optimal employee shift distribution using a modified Genetic Algorithm considering factors of productivity and risk of COVID-19 transmission (both medical and environmental). Check out our Scoovid [here](https://scoovid.herokuapp.com/)!

## Authors:
* [Glenda Emanuella Sutanto](https://gitlab.com/glendaesutanto)
* [Inigo Ramli](https://gitlab.com/IgoRamli)
* [Syams Ramadan](https://gitlab.com/syams.ramadan)
* [Wulan Mantiri](https://gitlab.com/wulanmantiri)

## Acknowledgements
* CS UI - Intelligent Systems B 2020
