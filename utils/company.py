from company.models import JobRequirement, Shift
from datetime import time
from hashlib import md5


def clean_shift_input(form_input, company):
    cleaned_data = []

    start_times = form_input.getlist('start_time', [])
    end_times =  form_input.getlist('end_time', [])
    for i in range(len(start_times)):
        try:
            start_time = int(start_times[i])
            end_time = int(end_times[i])
            if start_time >= end_time:
                continue
        except ValueError:
            continue

        for day in company.work_days:
            shift = Shift(
                start_time=time(start_time),
                end_time=time(end_time),
                day=day,
                company=company,
            )
            cleaned_data.append(shift)

    return cleaned_data


def clean_jobreq_input(form_input, company):
    cleaned_data = []

    job_types = form_input.getlist('job_type', [])
    num_of_employees =  form_input.getlist('num_of_employee', [])

    unique_job_reqs = {}
    for i in range(len(job_types)):
        job_type = int(job_types[i])
        num_of_employee = int(num_of_employees[i])
        try:
            unique_job_reqs[job_type] += num_of_employee
        except KeyError:
            unique_job_reqs[job_type] = num_of_employee

    for job_type, num_of_employee in unique_job_reqs.items():
        jobreq = JobRequirement(
            job_type_id=job_type,
            num_of_employee=num_of_employee,
            company=company,
        )
        cleaned_data.append(jobreq)

    return cleaned_data


def generate_secret_code(model_id):
    hash_md5 = md5(str(model_id).encode())
    return hash_md5.hexdigest()
