from .count_working_employees import count_working_employees


def get_team_formation(employee_schedule, shift, employees):
    team_formation = {}
    for i in range(len(employee_schedule)):
        if employee_schedule[i][shift]:
            job_type = employees[i].job_type_id
            try:
                team_formation[job_type] += 1
            except KeyError:
                team_formation[job_type] = 1
    return team_formation


def get_productivity(employee_schedule, company, employees, shifts, job_requirements):
    productivity = []

    for shift in range(shifts):
        if job_requirements:
            working_employees = 0
            team_formation = get_team_formation(employee_schedule, shift, employees)
            for job in job_requirements:
                job_type, num_of_employee = job
                try:
                    working_employees += min(team_formation[job_type], num_of_employee)
                except KeyError:
                    pass
        else:
            working_employees = count_working_employees(employee_schedule, shift)
            working_employees = min(working_employees, company.employees_per_shift)

        productivity_per_shift = round(working_employees / company.employees_per_shift, 2)
        productivity.append(productivity_per_shift * 100)

    return productivity
