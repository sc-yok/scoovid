from utils.calculate_medical_risk import total_medical_risk
from utils.calculate_environment_risk import total_environment_risk
from utils.count_working_employees import count_working_employees


def generate_transmission_table(employee_medical_history, employee_shift, start_time):
    header = [" "] + [str(start_time+i)+":00" for i in range (0, len(employee_shift[0]))]
    body = []
    for i in range (0, len(employee_medical_history)):
        medical_history = employee_medical_history[i]
        employee = medical_history.employee
        shift = employee_shift[i]
        row = {
            'header': employee.user.first_name + " " + employee.user.last_name,
            'content': [(calculate_total_risk(medical_history, employee_shift, j) if shift[j] else 0) for j in range (0, len(shift))]
        }
        body.append(row)

    return {
        'header': header,
        'body': body,
    }


def get_timetable_risk(medical_histories, employee_schedule, shifts) :
    risk_score = []
    individual_risks = []
    for i in range(0, len(medical_histories)):
        individual_risks.append([])
    for i in range (0, len(shifts)):
        shift_risk = 0
        active_people = 0
        safe = True
        working_hour = shifts[i].hours
        employee_encounter = count_working_employees(employee_schedule, i) #minus self employee
        for j in range (0, len(employee_schedule)):
            is_working = employee_schedule[j][i]
            medical_history = medical_histories[j]
            if is_working :
                employee_risk = calculate_total_risk(medical_history, employee_encounter, i, working_hour)
                individual_risks[j].append(employee_risk)
                if employee_risk == 100 :
                    safe = False
                    shift_risk = 100
                    break
                shift_risk += employee_risk
                active_people += 1
            else:
                individual_risks[j].append(-1.0)
        if active_people and safe:
            shift_risk = round(shift_risk/active_people,2)
        risk_score.append(shift_risk)

    risk = {
        'risk_score': risk_score,
        'individual_risks': individual_risks
    }
    return risk


def calculate_total_risk(medical_history, employee_encounter, shift_index, duration):
    medical_risk = total_medical_risk(medical_history)
    environment_risk = total_environment_risk(medical_history.employee, employee_encounter, duration)
    return round((100 if environment_risk >= 1 else (medical_risk*environment_risk)),2)
