from distutils.util import strtobool

def clean_medical_history_input(request_post):
    cleaned_input = {}
    for key in request_post:
        if key == "csrfmiddlewaretoken":
            continue
        cleaned_input[key] = strtobool(request_post[key])

    return cleaned_input