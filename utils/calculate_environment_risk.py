from constants.risk_percentage import risk_percentage

def total_environment_risk(employee, encounter, work_shift_duration):
    company = employee.company
    crowd_density = encounter/company.employee_capacity
    PROTECTION_LEVEL = 0.2 #assuming every employee is using a surgeon's mask
    encounter_points = get_encounter_points(encounter)
    work_duration_points = get_work_shift_duration_points(work_shift_duration)
    environment_risk_score = ((encounter_points + work_duration_points)/2) * crowd_density * (1-PROTECTION_LEVEL)
    return (0 if environment_risk_score<0 else environment_risk_score)
    
def get_encounter_points(encounter):
    if 1 <= encounter < 2 :
        return 1
    elif 2 <= encounter < 3 :
        return 2
    elif 3 <= encounter < 5 :
        return 3
    elif 5 <= encounter < 10 :
        return 4
    elif 10 <= encounter < 20 :
        return 5
    elif 20 <= encounter < 30 :
        return 6
    elif 30 <= encounter < 40 :
        return 6.5
    elif 40 <= encounter < 60 :
        return 7
    elif 60 <= encounter < 80 :
        return 8
    elif 80 <= encounter < 100 :
        return 9
    elif 100 <= encounter < 120 :
        return 9.5
    elif encounter >= 120 :
        return 10
        
def get_work_shift_duration_points(work_shift_duration):
    if 1 <= work_shift_duration < 2 :
        return 0.5
    elif 2 <= work_shift_duration < 4 :
        return 1
    elif 4 <= work_shift_duration < 6 :
        return 1.5
    elif 6 <= work_shift_duration < 8 :
        return 2
    elif 8 <= work_shift_duration < 10 :
        return 2.5
    elif 10 <= work_shift_duration < 12 :
        return 3
    elif 12 <= work_shift_duration < 24 :
        return 6
    elif 24 <= work_shift_duration < 30 :
        return 8
    elif work_shift_duration >= 30 :
        return 10