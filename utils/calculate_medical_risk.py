from constants.risk_percentage import risk_percentage

def calculate_disease_risk(request_post_data, disease):
    if request_post_data[disease]:
        return risk_percentage[disease]
    return 0.0

def calculate_age_risk(age):
    if age >= 80:
        return 14.8

    if age >= 70:
        return 8.0

    if age >= 60:
        return 3.6

    if age >= 50:
        return 1.3

    if age >= 40:
        return 0.4

    return 0.2

def total_medical_risk(medical_history):
    sum = (
        medical_history.age_risk +
        medical_history.cardiovascular +
        medical_history.diabetes +
        medical_history.chronic_respiratory +
        medical_history.hypertension +
        medical_history.cancer
    )

    return round((100*sum)/50.5, 2)