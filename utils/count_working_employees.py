def count_working_employees(employee_work_table, shift):
    employee_count = 0
    for employee in employee_work_table:
        employee_count += 1 if employee[shift] else 0
    return employee_count
