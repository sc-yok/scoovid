from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('general.urls')),
    path('', include('user.urls')),
    path('company/', include('company.urls')),
    path('employee/', include('employee.urls')),
]
