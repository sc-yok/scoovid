from django.utils.translation import gettext_lazy as _
from django.forms import (
    ModelForm,
    CharField,
    EmailInput,
    NumberInput,
    TextInput,
    TimeInput,
    Select,
)
from constants.form_attrs import (
    default_attrs,
    time_attrs,
)
from user.models import User
from user.forms import SignupForm
from .models import Company


class CompanyProfileForm(ModelForm):
    class Meta:
        model = Company
        fields = [
            'industry',
            'employee_capacity',
            'employees_per_shift',
            'start_operation_day',
            'end_operation_day',
        ]
        widgets = {
            'industry': Select(attrs=default_attrs),
            'employee_capacity': NumberInput(attrs=default_attrs),
            'employees_per_shift': NumberInput(attrs=default_attrs),
            'start_operation_day': Select(attrs=default_attrs),
            'end_operation_day': Select(attrs=default_attrs),
        }
        labels = {
            'employees_per_shift': _('Number of employees per shift:'),
        }


class CompanySecretCodeForm(ModelForm):
    secret_code = CharField(
        label=_("Company's Secret Code:"),
        widget=TextInput(attrs=default_attrs),
        required=True,
    )

    class Meta:
        model = Company
        fields = ['secret_code']
