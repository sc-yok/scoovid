from django.urls import path
from .views import (
    company_dashboard,
    create_company_profile,
    company_shifts_and_job_reqs,
    crud_secret_code,
    run_ga,
    transmission_table,
)

app_name = 'company'

urlpatterns = [
    path('dashboard/', company_dashboard, name='home'),
    path('profile/create/', create_company_profile, name='create_profile'),
    path('data/', company_shifts_and_job_reqs, name='shift_and_jobreq'),
    path('generate-schedule/', run_ga, name='run_ga'),
    path('secret-code/', crud_secret_code, name='secret_code'),
    path('transmission-table/', transmission_table, name='transmission_table'),
]
