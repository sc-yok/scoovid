from django.contrib import admin
from .models import Company, JobRequirement, Shift


class JobRequirementInline(admin.TabularInline):
    model = JobRequirement

class ShiftInline(admin.TabularInline):
    model = Shift

class CompanyAdmin(admin.ModelAdmin):
    inlines = [
        JobRequirementInline,
        ShiftInline,
    ]


admin.site.register(Company, CompanyAdmin)
