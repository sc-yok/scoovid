from django.shortcuts import render, redirect, reverse
from django.db.models import Prefetch
from datetime import time
from .forms import CompanyProfileForm
from .models import Company, JobRequirement, Shift

from ai import run_genetic_algorithm
from employee.models import Employee, EmployeeShift
from general.models import JobType
from user.models import User
from user.permissions import company_required
from utils.company import (
    clean_shift_input,
    clean_jobreq_input,
    generate_secret_code,
)


def get_company(user):
    return Company.objects.prefetch_related(
        'shift_set',
        'employee_set',
        'jobrequirement_set',
    ).get(user_id=user.id)


@company_required()
def run_ga(request):
    company = get_company(request.user)
    queryset = company.shift_set.prefetch_related('employeeshift_set').all()
    for shift in queryset: #deleting the already generated schedule if exist
        shift.employeeshift_set.all().delete()

    if company.employee_set.exists():
        run_genetic_algorithm(company)
    return redirect('company:home')


@company_required()
def company_dashboard(request):
    company = get_company(request.user)
    return get_company_schedule(request, company)


def get_company_schedule(request, company):
    raw_shifts = company.shift_set.prefetch_related('employeeshift_set').all()
    processed_shifts = convert_shift_array(raw_shifts)
    employees = company.employee_set.all()
    shift_sample = Shift.objects.filter(company=company,day=company.start_operation_day)
    return render(request, 'dashboard/dashboard.html', {
        'shifts':processed_shifts[0], 
        'employees':employees, 
        'shift_sample': shift_sample,
        'is_empty':processed_shifts[1],
        'schedule': 'company',
    })


def convert_shift_array(shifts):
    result = {}
    is_empty = True
    active_day = set()
    for shift in shifts:
        if (shift.day-1) not in active_day :
            result[shift.day-1] = {}
        current_employee_shift = shift.employeeshift_set.all()
        result[shift.day-1][shift] = current_employee_shift
        if len(current_employee_shift) >= 1:
            is_empty = False
        active_day.add(shift.day-1)
    return [result, is_empty]


@company_required()
def create_company_profile(request):
    form = CompanyProfileForm()
    if request.method == 'POST':
        form = CompanyProfileForm(request.POST)
        if form.is_valid():
            company = form.save()
            company.user = request.user
            company.secret_code = generate_secret_code(company.id)
            company.save()
            return redirect('company:shift_and_jobreq')

    return render(request, 'form/profile.html', {
        'action': 'create',
        'url': reverse('company:create_profile'),
        'form': form,
    })


@company_required()
def company_shifts_and_job_reqs(request):
    company = get_company(request.user)

    if request.method == 'POST':
        data = request.POST
        shifts = clean_shift_input(data, company)
        company.shift_set.all().delete()
        Shift.objects.bulk_create(shifts)

        job_requirements = clean_jobreq_input(data, company)
        company.jobrequirement_set.all().delete()
        JobRequirement.objects.bulk_create(job_requirements)

        if len(job_requirements) > 0:
            company.employees_per_shift = sum([j.num_of_employee for j in job_requirements])
            company.save()
        return redirect('company:home')
    else:
        job_types = JobType.objects.filter(industry_id=company.industry_id)
        job_type_choices = [{'id': j.id, 'name': j.name} for j in job_types]

        company_shifts = set()
        for s in company.shift_set.all():
            company_shifts.add((s.start_time.hour, s.end_time.hour))
        company_shifts = [
            {'start_time': start, 'end_time': end}
            for start, end in sorted(company_shifts)
        ]

        company_job_reqs = [
            {'job_type_id': j.job_type_id, 'num_of_employee': j.num_of_employee} 
            for j in company.jobrequirement_set.all()
        ]

    return render(request, 'form/shift_and_jobreq.html', {
        'url': reverse('company:shift_and_jobreq'),
        'job_type_choices': job_type_choices,
        'shifts': company_shifts,
        'job_reqs': company_job_reqs,
        'employees_per_shift': company.employees_per_shift,
    })


@company_required()
def crud_secret_code(request):
    company = request.user.company
    if not company.secret_code:
        company.secret_code = generate_secret_code(company.id)
        company.save()

    return render(request, 'company_secret_code.html', {
        'company': company
    })


@company_required()
def transmission_table(request):
    company = get_company(request.user)
    raw_shifts = company.shift_set.prefetch_related('employeeshift_set').all()
    processed_shifts = convert_shift_array_for_transmission_table(raw_shifts)
    employees = company.employee_set.all()
    return render(request, 'company_transmission_table.html', {
        'shifts':processed_shifts[0], 
        'employees':employees, 
        'is_empty': processed_shifts[1],
        'shift_sample': raw_shifts,
    })

def convert_shift_array_for_transmission_table(raw_shifts):
    result = []
    is_empty = True
    for raw_shift in raw_shifts:
        tmp = {}
        current_employee_shift = raw_shift.employeeshift_set.all()
        tmp[raw_shift] = current_employee_shift
        if len(current_employee_shift) >= 1:
            is_empty = False
        result.append(tmp)

    return [result, is_empty]
