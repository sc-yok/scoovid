from django.db import models
from hashlib import md5

from constants.model_choices import Day


class Company(models.Model):
    user = models.OneToOneField('user.User', on_delete=models.CASCADE, null=True)
    secret_code = models.CharField(max_length=100, unique=True, null=True)

    industry = models.ForeignKey('general.Industry', on_delete=models.SET_NULL, null=True)
    employee_capacity = models.IntegerField(default=0)
    employees_per_shift = models.IntegerField(default=0)
    start_operation_day = models.IntegerField(choices=Day.choices, default=Day.MONDAY)
    end_operation_day = models.IntegerField(choices=Day.choices, default=Day.FRIDAY)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @property
    def name(self):
        return self.user.get_full_name() if self.user else '-'

    @property
    def work_days(self):
        return range(self.start_operation_day, self.end_operation_day + 1)

    def __str__(self):
        return self.name


class JobRequirement(models.Model):
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    job_type = models.ForeignKey('general.JobType', on_delete=models.CASCADE)
    num_of_employee = models.IntegerField(default=0)

    def __str__(self):
        return f'{self.job_type.name} - {self.num_of_employee} employee(s)'


class Shift(models.Model):
    start_time = models.TimeField()
    end_time = models.TimeField()
    day = models.IntegerField(choices=Day.choices, null=True)
    company = models.ForeignKey('company.Company', on_delete=models.CASCADE)

    class Meta:
        ordering = ['day', 'start_time', 'end_time']

    @property
    def hours(self):
        return self.end_time.hour - self.start_time.hour

    def __str__(self):
        return f'{self.get_day_display()}: {self.get_hour_string()}'

    def get_hour_string(self):
        return f'{self.start_time.strftime("%H:%M")} - {self.end_time.strftime("%H:%M")}'
